//
//  VATReportViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 24/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
struct vat : Decodable{
    var vat_collected : String?
    var vatremitted : String?
    var gov : String?
}

class VATReportViewController: UIViewController,PopUp,UITableViewDelegate,UITableViewDataSource{
   
    @IBOutlet weak var btnVatFromDate: UIButton!
    @IBOutlet weak var btnVatToDate: UIButton!
    @IBOutlet weak var tblVat: UITableView!
    @IBOutlet weak var btnReportOutlet: UIButton!
    
    var buttonTag : Int?
    var userData : [LoginDetails] = [LoginDetails]()
    var vatData : [vat] = [vat]()
    
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
        }catch{
            print(error)
        }
        
        btnVatFromDate.layer.cornerRadius = 10
        btnVatFromDate.clipsToBounds = true
        
        btnVatToDate.layer.cornerRadius = 10
        btnVatToDate.clipsToBounds = true
        
        btnReportOutlet.layer.cornerRadius = 5
        btnReportOutlet.layer.borderWidth = 1
        btnReportOutlet.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        btnReportOutlet.clipsToBounds = true
    }
    
    
    func addDatePickerData(date: String) {
        if buttonTag == 0{
            btnVatFromDate.setTitle(date, for: .normal)
        }
        else if buttonTag == 1{
            btnVatToDate.setTitle(date, for: .normal)
        }
    }
    
    @IBAction func btnSelectVatFrom(_ sender: UIButton) {
        performSegue(withIdentifier: "vatrepostsegue", sender: self)
        self.buttonTag = 0
    }
    
    @IBAction func btnSelectVatTo(_ sender: UIButton) {
        performSegue(withIdentifier: "vatrepostsegue", sender: self)
        self.buttonTag = 1
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "vatrepostsegue"{
            if let vc = segue.destination as? PopUpPickerViewController{
                vc.delegate = self
            }
        }
    }
    
    @IBAction func btnVatReport(_ sender: UIButton) {
        if btnVatFromDate.titleLabel?.text != "From Date" && btnVatToDate.titleLabel?.text != "To Date"{
            getVatData()
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func getVatData(){
        let vatUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_vat.php"
        let params : [String:String] = ["username":userData[0].username!,"password":userData[0].password!,"from":(btnVatFromDate.titleLabel?.text)!,"to":(btnVatToDate.titleLabel?.text)!]
        Alamofire.request(vatUrl, method: .post, parameters: params).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                    self.vatData.removeAll()
                    let result = try JSONDecoder().decode([vat].self, from: res.data!)
                    self.vatData.append(vat(vat_collected: result[0].vat_collected, vatremitted: result[0].vatremitted, gov: result[0].gov))
                    self.tblVat.reloadData()
                }catch{
                print(error.localizedDescription)
                    let alert = UIAlertController(title: "Warning", message: "No Data Found...Invalid Date!!!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vatData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vatCell", for: indexPath) as! VatReportTableViewCell
        cell.lblCollctedVat.text = vatData[indexPath.row].vat_collected
        cell.lblRemitdVat.text = vatData[indexPath.row].vatremitted
        cell.lblGov.text = vatData[indexPath.row].gov
        return cell
    }
}
