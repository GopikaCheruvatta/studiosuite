//
//  HomePageViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 16/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct Details{
    var key : String?
    var value : Any?
}
struct Branch{
    var name : String?
    var email : String?
}

class HomePageViewController: BaseViewController, PopUp,UITableViewDelegate,UITableViewDataSource{
   
    var username : String?
    var password : String?
    var list : [Details] = [Details]()
    var branchData : [Branch] = [Branch]()
    var tag : Int?
    
    @IBOutlet weak var tblFirm: UITableView!
    
    @IBOutlet weak var btnFirm: UIButton!
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var btnTo: UIButton!
    @IBOutlet weak var btnFilterOutlet: UIButton!
    
    @IBOutlet weak var lblCllctdvat: UILabel!
    @IBOutlet weak var lblRmtdvat: UILabel!
    @IBOutlet weak var lblGov: UILabel!
    @IBOutlet weak var lblSales: UILabel!
    @IBOutlet weak var lblProfit: UILabel!
    @IBOutlet weak var lblPurchase: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblFirm.dataSource = self
        tblFirm.delegate = self
        
        tblFirm.isHidden = true
        btnFilterOutlet.isHidden = true
        
        LoadValue()
        addSlideMenuButton()
    }
    
    func addDatePickerData(date: String) {
        if tag == 0{
            btnFrom.setTitle(date, for: .normal)
        }
        else if tag == 1{
            btnTo.setTitle(date, for: .normal)
        }
    }
    
    func LoadValue(){
        let loadUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_admin_index1.php"
        Alamofire.request(loadUrl, method: .post, parameters: ["username" : username!,"password" : password!] ).responseJSON { (res) in
            if res.result.isSuccess{
                print("successs")
                do{
                    let answer = try JSONSerialization.jsonObject(with: res.data!) as AnyObject
                    print(answer)
                    if let status = answer["status"]{
                        self.lblGov.text = "0"
                        self.lblSales.text = "0"
                        self.lblProfit.text = "0"
                        self.lblRmtdvat.text = "0"
                        self.lblPurchase.text = "0"
                        self.lblCllctdvat.text = "0"
                        return
                    }
                    let ans = answer[0] as AnyObject
                    let data = JSON(ans)
                    self.lblCllctdvat.text = data["collectedvat"].stringValue
                    self.lblRmtdvat.text = data["remittedvat"].stringValue
                    self.lblGov.text = data["gov"].stringValue
                    self.lblSales.text = data["sales"].stringValue
                    self.lblProfit.text = data["profit"].stringValue
                    self.lblPurchase.text = data["purchase"].stringValue
                    }catch{
                    print("Error\(error.localizedDescription)")
                }
            }else{
                print("sdfgh")
            }
        }
    }
    
    @IBAction func btnFromDate(_ sender: UIButton) {
        performSegue(withIdentifier: "popup", sender: self)
        self.tag = 0
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "popup"{
            if let vc = segue.destination as? PopUpPickerViewController{
                vc.delegate = self
            }
        }else if segue.identifier == "menu"{
            if let mvc = segue.destination as? MenuViewController{
                mvc.uname = username
                mvc.pswd = password
            }
        }
    }
    @IBAction func btnToDate(_ sender: UIButton) {
        performSegue(withIdentifier: "popup", sender: self)
        self.tag = 1
    }
    
    @IBAction func btnSelectFirm(_ sender: UIButton) {
        if tblFirm.isHidden{
            animateTable(toggle: true)
        }else{
            animateTable(toggle: false)
        }
        fetchFirm()
    }
    func animateTable(toggle : Bool){
        if toggle{
            UIView.animate(withDuration: 0.3){
                self.tblFirm.isHidden = false
            }
        }else{
            UIView.animate(withDuration: 0.3){
                self.tblFirm.isHidden = true
            }
        }
    }
    
    @IBAction func btnFilter(_ sender: UIButton) {
        if btnFrom.titleLabel?.text != "From Date" && btnTo.titleLabel?.text != "To Date" && btnFirm.titleLabel?.text != "Select Firm"{
             filterData()
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func fetchFirm(){
        let firmUrl = "http://nuvactech-001-site13.etempurl.com/ios_hotel.php"
        print("loaded.....")
        Alamofire.request(firmUrl, method: .post, parameters: ["username" : username!,"password": password!]).responseJSON { (response) in
            print("Successssss")
            if response.result.isSuccess{
                do{
                    self.branchData.removeAll()
                    let result = try JSONSerialization.jsonObject(with: response.data!) as! [String : AnyObject]
                    let hotel = result["hotel"]!
                    let resultData = hotel as! [AnyObject]
                    print(resultData)
                  
                    for branches in resultData {
                        self.branchData.append(Branch(name: branches["name"]!! as? String, email: branches["email"]!! as? String))
                    }
                    self.tblFirm.reloadData()
                }catch{
                    print("Failed")
                }
            }
        }
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branchData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = branchData[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        btnFirm.setTitle(branchData[indexPath.row].name, for: .normal)
        animateTable(toggle: false)
        btnFilterOutlet.isHidden = false
    }
    func filterData(){
        let filterUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_admin_index1.php"
         let params : [String : String] = ["from" : (btnFrom.titleLabel?.text!)!, "to" : (btnTo.titleLabel?.text!)!,"client":(btnFirm.titleLabel?.text!)!,"username" : username!,"password":password!]
        Alamofire.request(filterUrl, method: .post, parameters: params).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                    let answer = try JSONSerialization.jsonObject(with: res.data!) as! [String : AnyObject]
                    if let status = answer["status"]{
                        self.lblGov.text = "0"
                        self.lblSales.text = "0"
                        self.lblProfit.text = "0"
                        self.lblRmtdvat.text = "0"
                        self.lblPurchase.text = "0"
                        self.lblCllctdvat.text = "0"
                        return
                    }
                    for (key,value) in answer["message"] as! [String : Any]{
                        self.list.append(Details(key: key, value: value))
                        print(key)
                        print(value)
                    }
                    self.lblCllctdvat.text = self.list[3].value as? String
                    self.lblRmtdvat.text = self.list[4].value as? String
                    self.lblGov.text = self.list[5].value as? String
                    self.lblSales.text = self.list[0].value as? String
                    self.lblProfit.text = self.list[2].value as? String
                    self.lblPurchase.text = self.list[1].value as? String
                }catch{
                    print("Error\(error.localizedDescription)")
                }
            }else{
                print("Failed")
            }
        }
    }
}
