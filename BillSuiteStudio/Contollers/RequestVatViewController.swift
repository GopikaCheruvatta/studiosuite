//
//  RequestVatViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 25/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct RequestVat : Decodable{
    var id : String?
    var details : String?
    var message : String?
    var fromdate : String?
    var todate : String?
    var email : String?
    var regno : String?
    var branch : String?
}

class RequestVatViewController: UIViewController {
    
    var regNo : String?
    var userData : [LoginDetails] = [LoginDetails]()
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    var requestArray : [RequestVat] = [RequestVat]()
    
    @IBOutlet weak var lblRegno: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblBranch: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
        }catch{
            print(error)
        }
        getRequestVat()
    }
    func getRequestVat(){
        let requesturl = "http://nuvactech-001-site13.etempurl.com/ios_requestvat.php"
        print("loadingggg")
        let params : [String : String] = ["username":userData[0].username!,"password":userData[0].password!,"regno":regNo!]
        Alamofire.request(requesturl, method: .post, parameters: params).responseJSON { (res) in
            do{
                print(self.regNo!)
                let result = try JSONSerialization.jsonObject(with: res.data!) as AnyObject
                let answer = result["data"]! as AnyObject
                for key in answer as! [AnyObject]{
                 print( key["id"] as! String)
                    self.requestArray.append(RequestVat(id: key["id"] as? String, details: key["details"] as? String, message: key["message"] as? String, fromdate: key["fromdate"] as! String, todate: key["todate"] as? String, email: key["email"] as? String, regno: key["regno"] as! String, branch: key["branch"] as? String))
                }
                self.lblRegno.text = self.requestArray[0].regno
                self.lblEmail.text = self.requestArray[0].email
                self.lblBranch.text = self.requestArray[0].branch
                self.lblToDate.text = self.requestArray[0].todate
                self.lblDetails.text = self.requestArray[0].details
                self.lblMessage.text = self.requestArray[0].message
                self.lblFromDate.text = self.requestArray[0].fromdate
            }catch{
                print(error.localizedDescription)
            }
        }
    }
}
