//
//  PurchaseReportViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 21/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct Purchase : Decodable {
    var id : String?
    var invoiceno : String?
    var amount : String?
    var vat : String?
    var paymentmode : String?
    var remark : String?
    var docname : String?
    var branchid : String?
    var status : String?
}
struct allPurchase : Decodable{
    var purchase : [Purchase]
}
struct Sales : Decodable {
    var Invoice_Number : String?
    var amount : String?
    var client_id : String?
    var id : String?
    var staff_id : String?
    var status : String?
    var table_id : String?
    var vat_total : String?
}

class PurchaseReportViewController: UIViewController ,PopUp,passMerchant,passHotel,UITableViewDelegate,UITableViewDataSource{
    func passHotelList(hotelName: String) {
        btnMerchant.setTitle(hotelName, for: .normal)
    }
   
    func passMerchantName(name: String) {
        btnMerchant.setTitle(name, for: .normal)
    }
    
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var btnMerchant: UIButton!
    @IBOutlet weak var btnTo: UIButton!
    @IBOutlet weak var PurchaseTable: UITableView!
    @IBOutlet weak var lblTableTitle: UILabel!
    @IBOutlet weak var btnReportOutlet: UIButton!
    
    var tag : Int?
    var purchaseData : [Purchase] = [Purchase]()
    var userData : [LoginDetails] = [LoginDetails]()
    var flag : Int?
    var salesData : [Sales] = [Sales]()
    
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")

    override func viewDidLoad() {
        super.viewDidLoad()

        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
        }catch{
            print(error)
        }
        PurchaseTable.isHidden = true
        
        btnFrom.layer.cornerRadius = 5
        btnFrom.clipsToBounds = true
        
        btnTo.layer.cornerRadius = 5
        btnTo.clipsToBounds = true
        
        btnMerchant.layer.cornerRadius = 5
        btnMerchant.clipsToBounds = true
        
        btnReportOutlet.layer.cornerRadius = 5
        btnReportOutlet.clipsToBounds = true
        btnReportOutlet.layer.borderWidth = 1
        btnReportOutlet.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        
        setSelectButtonTitle()
        setTableTitleLabel()
        
    }
    func addDatePickerData(date: String) {
        if tag == 0{
            btnFrom.setTitle(date, for: .normal)
        }
        else if tag == 1{
            btnTo.setTitle(date, for: .normal)
        }
    }
    
    func setSelectButtonTitle(){
        if flag == 0{
            btnMerchant.setTitle("Select Merchant", for: .normal)
        }else if flag == 1{
            btnMerchant.setTitle("Select Hotel", for: .normal)
        }
    }
    func setTableTitleLabel(){
        if flag == 0{
            lblTableTitle.text = "PURCHASE REPORT"
        }
        else if flag == 1{
            lblTableTitle.text = "SALES REPORT"
        }
    }
    @IBAction func btnSelectMerchant(_ sender: UIButton) {
      performSegue(withIdentifier: "clientPopup", sender: self)
    }
    
    @IBAction func btnReport(_ sender: UIButton) {
        if flag == 0{
            if btnFrom.titleLabel?.text != "From Date" && btnTo.titleLabel?.text != "To Date" && btnMerchant.titleLabel?.text != "Select Merchant"{
                getPurchaseData()
            }
            else{
                let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            }
        }
        else if flag == 1{
            if btnFrom.titleLabel?.text != "From" && btnTo.titleLabel?.text != "To" && btnMerchant.titleLabel?.text != "Select Hotel"{
                 getSalesData()
            }
            else{
                let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    @IBAction func btnFromDate(_ sender: UIButton) {
        performSegue(withIdentifier: "purchase", sender: self)
        self.tag = 0
    }
    
    @IBAction func btnToDate(_ sender: UIButton) {
        performSegue(withIdentifier: "purchase", sender: self)
        self.tag = 1
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "purchase"{
            if let vc = segue.destination as? PopUpPickerViewController{
                vc.delegate = self
            }
        }
        else if segue.identifier == "clientPopup"{
            if let clientVc = segue.destination as? MerchantPickerViewController{
                clientVc.delegate = self
                clientVc.delegateHotel = self 
                clientVc.selectionId = flag
            }
        }
    }
    
    func getPurchaseData(){
        let purchaseUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_purchase_report.php"
        let params : [String:String] = ["username": userData[0].username!,"password":userData[0].password!,"from":(btnFrom.titleLabel?.text)!,"to":(btnTo.titleLabel?.text)!,"client":(btnMerchant.titleLabel?.text)!]
        Alamofire.request(purchaseUrl, method: .post, parameters: params).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                    let result = try JSONDecoder().decode(allPurchase.self, from: res.data!)
                    print(result.purchase)
                    for ans in result.purchase{
                        self.purchaseData.append(Purchase(id: ans.id, invoiceno: ans.invoiceno, amount: ans.amount, vat: ans.vat, paymentmode: ans.paymentmode, remark: ans.remark, docname: ans.docname, branchid: ans.branchid, status: ans.status))
                        print(self.purchaseData)
                    }
                     self.PurchaseTable.isHidden = false
                    self.PurchaseTable.reloadData()
                }catch{
                    print(error.localizedDescription)
                        let alert = UIAlertController(title: "Warning", message: "No Data Found...Invalid Date!!!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                }
            }
        }
    }
    func getSalesData(){
        let saleUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_sales_report.php"
        Alamofire.request(saleUrl, method: .post, parameters: ["username" : userData[0].username!,"password" : userData[0].password!,"from" : (btnFrom.titleLabel?.text)!,"to" :(btnTo.titleLabel?.text)!,"client" : (btnMerchant.titleLabel?.text)!]).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as! AnyObject
                    print(result)
                    if let status = result["status"]!{
                        let alert = UIAlertController(title: "Warning", message: "No Data Found", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                        return
                    }
                    let ans = result["sales"] as! [AnyObject]
                    for answer in ans{
                        self.salesData.append(Sales(Invoice_Number: answer["Invoice_Number"] as? String, amount: answer["amount"] as? String, client_id: answer["client_id"] as? String, id: answer["id"] as? String, staff_id: answer["staff_id"] as? String, status: answer["status"] as? String, table_id: answer["table_id"] as? String, vat_total: answer["vat_total"] as? String))
                    }
                    self.PurchaseTable.isHidden = false
                    self.PurchaseTable.reloadData()
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount : Int?
        if flag == 0{
            rowCount = purchaseData.count
        }
        else if flag == 1{
            rowCount = salesData.count
        }
        return rowCount!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseCell", for: indexPath) as? PurchaseDetailsTableViewCell
        if flag == 0{
            cell?.lblInvoiceNo.text = purchaseData[indexPath.row].invoiceno
            cell?.lblAmount.text = purchaseData[indexPath.row].amount
            cell?.lblVAT.text = purchaseData[indexPath.row].vat
            cell?.lblPaymentMode.text = purchaseData[indexPath.row].paymentmode
            cell?.lblRemark.text = purchaseData[indexPath.row].remark
        }else if flag == 1{
            cell?.lblInvoiceNo.text = salesData[indexPath.row].Invoice_Number
            cell?.lblAmount.text = salesData[indexPath.row].amount
            cell?.lblVAT.text = salesData[indexPath.row].vat_total
            cell?.lblRemark.text = salesData[indexPath.row].status
            
            cell?.labelPayment.isHidden = true
            cell?.lblPaymentMode.isHidden = true
        }
        return cell!
    }
}
