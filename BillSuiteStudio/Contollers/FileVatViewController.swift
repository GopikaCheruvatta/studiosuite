//
//  FileVatViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 25/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct fileVat : Decodable{
    var regno : String?
}

class FileVatViewController: UIViewController,PopUp,passHotel {
  
    @IBOutlet weak var btnFromVat: UIButton!
    @IBOutlet weak var btnToVat: UIButton!
    @IBOutlet weak var btnHotel: UIButton!
    
    @IBOutlet weak var btnSaveOutlet: UIButton!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var txtDetails: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    var userData : [LoginDetails] = [LoginDetails]()
    var btntag : Int?
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    var regnum : String?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
        }catch{
            print(error)
        }
        btnRequest.isHidden = true
        
        btnHotel.layer.cornerRadius = 10
        btnHotel.clipsToBounds = true
       
        btnToVat.layer.cornerRadius = 10
        btnToVat.clipsToBounds = true
        
        btnFromVat.layer.cornerRadius = 10
        btnFromVat.clipsToBounds = true
        
        btnRequest.layer.cornerRadius = 10
        btnRequest.layer.borderWidth = 1
        btnRequest.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        btnRequest.clipsToBounds = true
        
        btnSaveOutlet.layer.cornerRadius = 10
        btnSaveOutlet.layer.borderWidth = 1
        btnSaveOutlet.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        btnSaveOutlet.clipsToBounds = true
    }
    
    func addDatePickerData(date: String) {
        if btntag == 0{
            btnFromVat.setTitle(date, for: .normal)
        }
        else if btntag == 1{
            btnToVat.setTitle(date, for: .normal)
        }
    }
    func passHotelList(hotelName: String) {
        btnHotel.setTitle(hotelName, for: .normal)
    }
    
    @IBAction func btnSelectFrom_vat(_ sender: UIButton) {
        performSegue(withIdentifier: "filevatDate", sender: self)
        self.btntag = 0
    }
    
    @IBAction func btnSelectTo_vat(_ sender: UIButton) {
        performSegue(withIdentifier: "filevatDate", sender: self)
        self.btntag = 1
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filevatDate"{
            if let vc = segue.destination as? PopUpPickerViewController{
                vc.delegate = self
                vc.dateIdFormat = 1
            }
        }
        else if segue.identifier == "filevathotel"{
            if let hotelVc = segue.destination as? MerchantPickerViewController{
                hotelVc.delegateHotel = self
                hotelVc.selectionId = 1
            }
        }
        else if segue.identifier == "request"{
            if let requestVc = segue.destination as? RequestVatViewController{
                requestVc.regNo = regnum!
            }
        }
    }
    
    @IBAction func btnSelectHotel(_ sender: UIButton) {
        performSegue(withIdentifier: "filevathotel", sender: self)
    }
    
    @IBAction func btnSaveVat(_ sender: UIButton) {
        if btnFromVat.titleLabel?.text != "From Date" && btnToVat.titleLabel?.text != "To Date" && btnHotel.titleLabel?.text != "Select Hotel" && txtEmail.text != "" && txtDetails.text != "" && txtMessage.text != ""{
            getRegno()
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnRequestVat(_ sender: UIButton) {
        performSegue(withIdentifier: "request", sender: self)
    }
    
    func getRegno(){
        let fileUrl = "http://nuvactech-001-site13.etempurl.com/ios_filevat.php"
        let params : [String : String] = ["username":userData[0].username!,"password": userData[0].password!,"from":(btnFromVat.titleLabel?.text)!,"to":(btnToVat.titleLabel?.text)!,"hotel":(btnHotel.titleLabel?.text)!,"details":txtDetails.text!,"message":txtMessage.text!,"email":txtEmail.text!]
        Alamofire.request(fileUrl, method: .post, parameters: params).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                   let result = try JSONDecoder().decode(fileVat.self, from: res.data!)
                    self.regnum = result.regno!
                    print(self.regnum!)
                    let alert = UIAlertController(title: "Success", message: "You are Successfully Registered", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (response) in
                        self.btnRequest.isHidden = false
                    }))
                    self.present(alert, animated: true, completion: nil)
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
}
