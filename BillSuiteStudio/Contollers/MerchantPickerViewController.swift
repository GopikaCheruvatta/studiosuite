//
//  MerchantPickerViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 22/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct client : Decodable{
    var merchant : String?
}
struct hotel : Decodable{
    var name : String?
    var email : String?
}
protocol passMerchant{
    func passMerchantName(name : String)
}
protocol passHotel {
    func passHotelList(hotelName : String)
}
class MerchantPickerViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
   
    var userData : [LoginDetails] = [LoginDetails]()
    var data : [client] = [client]()
    var clientName : String?
    var delegate : passMerchant?
    var hotelList : [hotel] = [hotel]()
    var selectionId : Int?
    var delegateHotel : passHotel?
    var hotelData : String?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var MerchantPicker: UIPickerView!
    @IBOutlet weak var btnSave: UIButton!
    
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        MerchantPicker.delegate = self
        MerchantPicker.dataSource = self
      
        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
            
        }catch{
            print(error)
        }
        
        MerchantPicker.layer.cornerRadius = 5
        MerchantPicker.clipsToBounds = true
    
        if selectionId == 0{
            lblTitle.text = "Merchant List"
        }else if selectionId == 1{
            lblTitle.text = "Hotel List"
        }
        selectAlamofireRequest()
        
    }
    func selectAlamofireRequest(){
        if selectionId == 0{
            GetMerchant()
        }else if selectionId == 1{
            getHotelList()
        }
    }

    @IBAction func btnSaveMerchant(_ sender: UIButton) {
        if selectionId == 0{
            delegate?.passMerchantName(name: clientName!)
            dismiss(animated: true, completion: nil)

        }else if selectionId == 1{
            delegateHotel?.passHotelList(hotelName: hotelData!)
            dismiss(animated: true)
        }
          }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var rowCount : Int = 0
        if selectionId == 0{
            rowCount = data.count
        }
        else if selectionId == 1{
            rowCount = hotelList.count
        }
        return rowCount
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var answer : String?
        if selectionId == 0{
             answer = data[row].merchant
        }
        else if selectionId == 1{
           answer = hotelList[row].name
        }
        return answer
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if selectionId == 0{
                    clientName = data[row].merchant
            }else if selectionId == 1{
                hotelData = hotelList[row].name
        }
    }
    
    func GetMerchant(){
        let clientUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_merchant.php"
        print("success")
        Alamofire.request(clientUrl, method: .post, parameters: ["username":userData[0].username!,"password":userData[0].password!]).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                    self.data = try JSONDecoder().decode([client].self, from: res.data!)
                    print(self.data)
                    self.MerchantPicker.reloadAllComponents()
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func getHotelList(){
        let hotelUrl = "http://nuvactech-001-site13.etempurl.com/ios_hotel.php"
        print("loaded.....")
        Alamofire.request(hotelUrl, method: .post, parameters: ["username" : userData[0].username!,"password": userData[0].password!]).responseJSON { (response) in
            print("Successssssssss")
            if response.result.isSuccess{
                do{
                    let result = try JSONSerialization.jsonObject(with: response.data!) as! [String : AnyObject]
                    let ans = result["hotel"]!
                    let resultData = ans as! [AnyObject]
                    print(resultData)
                    for hotels in resultData {
                        self.hotelList.append(hotel(name: hotels["name"]!! as! String, email: hotels["email"]!! as! String))
                        print(self.hotelList)
                    }
                    self.MerchantPicker.reloadAllComponents()
                }catch{
                    print("Failed")
                }
            }
        }
    }
}
