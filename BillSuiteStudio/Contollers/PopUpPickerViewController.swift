//
//  PopUpPickerViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 18/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
protocol PopUp {
    func addDatePickerData(date : String)
}
class PopUpPickerViewController: UIViewController{
    
    @IBOutlet weak var pickerDate: UIDatePicker!
    @IBOutlet weak var btnSaveDate: UIButton!
    var date  : String?
    var delegate : PopUp?
    var dateIdFormat : Int?
    var currentDate : String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        if dateIdFormat == 1{
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            date = format.string(from:  pickerDate.date)
        }else{
            let format = DateFormatter()
            format.dateFormat = "dd-MM-yyyy"
            date = format.string(from:  pickerDate.date)
        }
        
        pickerDate.layer.cornerRadius = 10
        pickerDate.clipsToBounds = true
        
        currentDate =  MaxDate()
    }
    
    @IBAction func saveDateButton(_ sender: UIButton) {
        delegate?.addDatePickerData(date: date!)
        dismiss(animated: true)
    }
    
    @IBAction func selectedIDatePickerItem(_ sender: UIDatePicker) {
        if dateIdFormat == 1{
            let format = DateFormatter()
              format.dateFormat = "dd-MM-yyyy"
            if currentDate > format.string(from: pickerDate.date){
                format.dateFormat = "yyyy-MM-dd"
                date = format.string(from:  pickerDate.date)
            }
            else{
                format.dateFormat = "yyyy-MM-dd"
                date = currentDate
            }
        }
        else{
            let format = DateFormatter()
            format.dateFormat = "dd-MM-yyyy"
            if currentDate > format.string(from: pickerDate.date){
                date = format.string(from:  pickerDate.date)
            }
            else{
                date = currentDate
            }
        }
    }
    func MaxDate()-> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let result = formatter.string(from: date)
        print(result)
        return result
    }
}
