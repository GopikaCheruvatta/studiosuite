//
//  SettingViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 26/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
class SettingViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var btnChange: UIButton!
    
    var userData : [LoginDetails] = [LoginDetails]()
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
        }catch{
            print(error)
        }
        btnChange.layer.cornerRadius = 10
        btnChange.clipsToBounds = true
    }
    @IBAction func btnChangePassword(_ sender: UIButton) {
        if txtUsername.text != "" && txtNewPassword.text != "" && txtNewPassword.text != ""{
            ChangePassword()
        }else{
            let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func ChangePassword(){
        let changeUrl = "http://nuvactech-001-site13.etempurl.com/ios_settings.php"
        Alamofire.request(changeUrl, method: .post, parameters: ["username":userData[0].username!,"password":userData[0].password!,"newpassword":txtNewPassword.text!]).responseJSON { (res) in
            if res.result.isSuccess{
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as AnyObject
                    print(result["status"])
                    let alert = UIAlertController(title: "MESSAGE", message: result["status"] as! String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (respoonse) in
                        self.performSegue(withIdentifier: "setting_login", sender: self)
                    }))
                    self.present(alert,animated: true,completion: nil)
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
   
}
