//
//  HistoryFileViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 26/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
struct History  {
    var id :  String?
    var branch : String?
    var fromdate : String?
    var todate : String?
    var status : String?
    var regno : String?
}
class HistoryFileViewController: UIViewController,PopUp,passHotel,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var btnHistoryFrom: UIButton!
    @IBOutlet weak var btnHistoryTo: UIButton!
    @IBOutlet weak var btnHotel: UIButton!
    @IBOutlet weak var tableHistory: UITableView!
    @IBOutlet weak var btnHistory: UIButton!
    
    var historyDetails : [History] = [History]()
    var buttonFlag : Int?
    var userData : [LoginDetails] = [LoginDetails]()
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableHistory.delegate = self
        tableHistory.dataSource = self
        
        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            userData = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(userData[0].username!)
            print(userData[0].password!)
        }catch{
            print(error)
        }
        btnHistory.isHidden = true
        tableHistory.isHidden = true
        
        btnHistoryFrom.layer.cornerRadius = 10
        btnHistoryFrom.clipsToBounds = true
        
        btnHistoryTo.layer.cornerRadius = 10
        btnHistoryTo.clipsToBounds = true
        
        btnHotel.layer.cornerRadius = 10
        btnHotel.clipsToBounds = true
        
        btnHistory.layer.cornerRadius = 10
        btnHistory.layer.borderWidth = 1
        btnHistory.layer.borderColor = UIColor.black.cgColor
        btnHistory.clipsToBounds = true
        
    }
    func addDatePickerData(date: String) {
        if buttonFlag == 0{
            btnHistoryFrom.setTitle(date, for: .normal)
        }
        else if buttonFlag == 1{
            btnHistoryTo.setTitle(date, for: .normal)
        }
    }
    func passHotelList(hotelName: String) {
        btnHotel.setTitle(hotelName, for: .normal)
        btnHistory.isHidden = false
    }

    @IBAction func btnSelectHistory_FromDate(_ sender: UIButton) {
       performSegue(withIdentifier: "historyDate", sender: self)
        buttonFlag = 0
    }
    
    @IBAction func btnSelectHistory_ToDate(_ sender: UIButton) {
        performSegue(withIdentifier: "historyDate", sender: self)
        buttonFlag = 1
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "historyDate"{
            if let vc = segue.destination as? PopUpPickerViewController{
                vc.delegate = self
                vc.dateIdFormat = 1
            }
        }
        else if segue.identifier == "historyfileVat"{
            if let historyVc = segue.destination as? MerchantPickerViewController{
                historyVc.delegateHotel = self
                historyVc.selectionId = 1
            }
        }
    }
    
    @IBAction func btnSelectHotel(_ sender: UIButton) {
        performSegue(withIdentifier: "historyfileVat", sender: self)
        
    }
    
    @IBAction func btnShowHistory(_ sender: UIButton) {
        if btnHistoryFrom.titleLabel?.text != "From Date" && btnHistoryTo.titleLabel?.text != "To Date" && btnHotel.titleLabel?.text != "Select Hotel"{
            getHistoryFileVat()
            tableHistory.isHidden = false
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Please Fill the Details", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as? HistoryTableViewCell
        cell?.lblRegNo.text = historyDetails[indexPath.row].regno
        cell?.lblBranch.text = historyDetails[indexPath.row].branch
        cell?.lblFromDate.text = historyDetails[indexPath.row].fromdate
        cell?.lblToDate.text = historyDetails[indexPath.row].todate
        cell?.lblStatus.text = historyDetails[indexPath.row].status
        return cell!
    }
    func getHistoryFileVat(){
        let historyUrl = "http://nuvactech-001-site13.etempurl.com/ios_history.php"
        Alamofire.request(historyUrl, method: .post, parameters: ["username":userData[0].username!,"password":userData[0].password!,"from":(btnHistoryFrom.titleLabel?.text)!,"to":(btnHistoryTo.titleLabel?.text)!,"hotel":(btnHotel.titleLabel?.text)!]).responseJSON { (response) in
            if response.result.isSuccess{
                do{
                    let result = try JSONSerialization.jsonObject(with: response.data!) as AnyObject
                    let answer = result["history"]!
                    for ans in answer as! [AnyObject]{
                        print(ans["status"]!)
                        self.historyDetails.append(History(id: ans["id"] as? String, branch: ans["branch"] as? String, fromdate: ans["fromdate"] as? String, todate: ans["todate"] as? String, status: ans["status"] as? String, regno: ans["regno"] as? String))
                    }
                    self.tableHistory.reloadData()
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
}
