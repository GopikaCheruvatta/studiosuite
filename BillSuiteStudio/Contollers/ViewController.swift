//
//  ViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 16/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
struct User : Decodable{
    var status : String?
    var description : String?
    var username : String?
    var password : String?
}
struct LoginDetails : Encodable, Decodable {
    var username : String?
    var password : String?
}
class ViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    
    let url = "http://nuvactech-001-site13.etempurl.com/ios_new_login.php"
    var usrname : String?
    var passwrd : String?
    
    var defaults = UserDefaults.standard
    var loginData : [LoginDetails] = [LoginDetails]()
    var filePath = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    var textId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         print(FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask))
        
        btnLoginOutlet.layer.cornerRadius = 5
        btnLoginOutlet.layer.borderWidth = 1
        btnLoginOutlet.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        txtUsername.attributedPlaceholder = NSAttributedString(string: "Username",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtUsername.setBottomBorder()
        txtPassword.setBottomBorder()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func encodeData(){
        let encoder = PropertyListEncoder()
        do{
            let encodedData = try encoder.encode(loginData)
            try encodedData.write(to: self.filePath!)
        }catch{
            print("Error while encoding \(error.localizedDescription)")
        }
    }
    @IBAction func btnLogin(_ sender: UIButton) {
        login()
        let logData = LoginDetails(username: txtUsername.text!, password: txtPassword.text!)
        loginData.append(logData)
        encodeData()
    }
    func login(){
        Alamofire.request(url, method: .post, parameters: ["username" : txtUsername.text!, "password" : txtPassword.text!, "appcategory" : "BillSuite-ExcellentStudio"]).responseJSON { (response) in
            print("loaded")
            if response.result.isSuccess{
                print("success")
                do{
                    let result = try JSONDecoder().decode(User.self, from: response.data!)
                    print(result)
                    if result.status == "true"{
                        self.usrname = result.username
                        self.passwrd = result.password
                        self.performSegue(withIdentifier: "home", sender: self)
                    }
                }catch{
                    print("Error while\(error.localizedDescription)")
                }
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? HomePageViewController{
            vc.username = usrname
            vc.password = passwrd
        }
    }
}

extension UITextField {
    func setBottomBorder()
    {
        let border = CALayer()
        let width = CGFloat(2)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
