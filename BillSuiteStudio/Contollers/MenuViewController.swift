//
//  MenuViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 17/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var menuTable: UITableView!
    
    var uname : String?
    var pswd : String?
    var delegate : SlideMenuDelegate?
    var btnMenu : UIButton!
    let menu = ["View Staff","Purchase Report","Sales Report","VAT Report","File VAT","History","Settings","Logout"]
    var cellId : Int?
    var showId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menuTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = menu[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if menu[indexPath.row] == "View Staff"{
           performSegue(withIdentifier: "viewStaffCell", sender: self)
        }else if menu[indexPath.row] == "Purchase Report" {
            self.cellId = 0
            performSegue(withIdentifier: "purchase", sender: self)
        }else if menu[indexPath.row] == "Sales Report"{
            self.cellId = 1
            performSegue(withIdentifier: "purchase", sender: self)
        }else if menu[indexPath.row] == "VAT Report" {
            performSegue(withIdentifier: "vatreport", sender: self)
        }else if menu[indexPath.row] == "File VAT" {
            performSegue(withIdentifier: "filevat", sender: self)
        }else if menu[indexPath.row] == "History" {
            performSegue(withIdentifier: "history", sender: self)
        }else if menu[indexPath.row] == "Settings" {
            performSegue(withIdentifier: "setting", sender: self)
        }else if menu[indexPath.row] == "Logout" {
            performSegue(withIdentifier: "logoutsegue", sender: self)
            self.showId = 1
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "purchase"{
            if let vc = segue.destination as? PurchaseReportViewController{
                vc.flag = cellId
            }
        }
        else if segue.identifier == "logoutsegue"{
            if let logoutVc = segue.destination as? ViewController{
                logoutVc.textId = showId
            }
        }
        
    }
}
