//
//  ViewStaffViewController.swift
//  BillSuiteStudio
//
//  Created by macos on 20/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct ViewStaff {
    var Image : String?
    var Name : String?
    var Gender : String?
    var Address : String?
    var Phone : String?
    var Designation : String?
}
class ViewStaffViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   
    @IBOutlet weak var CollectionStaff: UICollectionView!
    var data : [LoginDetails] = [LoginDetails]()
    var staffDetails : [ViewStaff] = [ViewStaff]()
    
    let filePath =  FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            data = try decoder.decode([LoginDetails].self, from: dataToDecode)
            print(data[0].username!)
            print(data[0].password!)
            
        }catch{
            print(error)
        }
        LoadStaffDetails()
    }
    func base64Convert(base64String: String?) -> UIImage{
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            let temp = base64String?.components(separatedBy: ",")
            let dataDecoded : Data = Data(base64Encoded: temp![0], options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage!
        }
    }
    func LoadStaffDetails(){
        let staffUrl = "http://nuvactech-001-site13.etempurl.com/ios_new_viewstaff.php"
        Alamofire.request(staffUrl, method: .post, parameters: ["username" : data[0].username!,"password":data[0].password!]).responseJSON { (response) in
            if response.result.isSuccess{
                do{
                    print("loadingggg")
                    let staff = try JSONSerialization.jsonObject(with: response.data!) as AnyObject
                    let data = staff["staff"] as AnyObject
                    let ans = data as! [AnyObject]
                    for dataa in ans{
                        print(dataa["id"]!!)
                        self.staffDetails.append(ViewStaff(Image: dataa["Image"]!! as? String ,Name: dataa["Name"]!! as? String , Gender: dataa["Gender"]!! as? String, Address: dataa["Address"]!! as? String, Phone: dataa["Phone"]!! as? String, Designation: dataa["Designation"]!! as? String))
                        print(self.staffDetails[0].Name!)
                    }
                    self.CollectionStaff.reloadData()
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return staffDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! StaffDetailsCollectionViewCell
        
        cell.lblStaffName.text = staffDetails[indexPath.row].Name
        cell.lblPhone.text = staffDetails[indexPath.row].Phone
        cell.lblGender.text = staffDetails[indexPath.row].Gender
        cell.lblAddress.text = staffDetails[indexPath.row].Address
        cell.lblDesignation.text = staffDetails[indexPath.row].Designation
        cell.imgStaff.image = base64Convert(base64String: staffDetails[indexPath.row].Image)
        
        cell.imgStaff.layer.cornerRadius = 10
        cell.imgStaff.clipsToBounds = true
        return cell
    }
}

