//
//  VatReportTableViewCell.swift
//  BillSuiteStudio
//
//  Created by macos on 24/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class VatReportTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCollctedVat: UILabel!
    @IBOutlet weak var lblRemitdVat: UILabel!
    @IBOutlet weak var lblGov: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
