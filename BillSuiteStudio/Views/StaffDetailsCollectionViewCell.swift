//
//  StaffDetailsCollectionViewCell.swift
//  BillSuiteStudio
//
//  Created by macos on 20/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class StaffDetailsCollectionViewCell: UICollectionViewCell {
   
    
    @IBOutlet weak var imgStaff: UIImageView!
    @IBOutlet weak var lblStaffName: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
}
