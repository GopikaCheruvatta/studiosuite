//
//  PurchaseDetailsTableViewCell.swift
//  BillSuiteStudio
//
//  Created by macos on 23/09/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class PurchaseDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblInvoiceNo: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblVAT: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblRemark: UILabel!
    
    @IBOutlet weak var labelInvoice: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelVat: UILabel!
    @IBOutlet weak var labelPayment: UILabel!
    @IBOutlet weak var labelRemark: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
